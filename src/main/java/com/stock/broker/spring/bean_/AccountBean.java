package com.stock.broker.spring.bean_;

import java.io.Serializable;

public class AccountBean implements Serializable {

	private static final long serialVersionUID = -1453816495398100585L;

	private String userName;
	private String pazzWord;
	private String pazzWordConfirm;
	private String email;
	private int idQue;
	private String contentQue;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPazzWord() {
		return pazzWord;
	}

	public void setPazzWord(String pazzWord) {
		this.pazzWord = pazzWord;
	}

	public String getPazzWordConfirm() {
		return pazzWordConfirm;
	}

	public void setPazzWordConfirm(String pazzWordConfirm) {
		this.pazzWordConfirm = pazzWordConfirm;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdQue() {
		return idQue;
	}

	public void setIdQue(int idQue) {
		this.idQue = idQue;
	}

	public String getContentQue() {
		return contentQue;
	}

	public void setContentQue(String contentQue) {
		this.contentQue = contentQue;
	}

}
