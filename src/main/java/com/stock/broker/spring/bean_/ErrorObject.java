package com.stock.broker.spring.bean_;

public class ErrorObject {
	private boolean isValidate = true;
	private String msg;

	public boolean isValidate() {
		return isValidate;
	}

	public void setValidate(boolean isValidate) {
		this.isValidate = isValidate;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
