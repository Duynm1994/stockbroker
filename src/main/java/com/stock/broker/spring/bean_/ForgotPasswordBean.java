package com.stock.broker.spring.bean_;

public class ForgotPasswordBean {

	private String userName;
	private String email;
	private int idQue;
	private String contentQue;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContentQue() {
		return contentQue;
	}

	public void setContentQue(String contentQue) {
		this.contentQue = contentQue;
	}

	public int getIdQue() {
		return idQue;
	}

	public void setIdQue(int idQue) {
		this.idQue = idQue;
	}

}
