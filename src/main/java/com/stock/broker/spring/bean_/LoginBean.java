package com.stock.broker.spring.bean_;

import java.io.Serializable;

public class LoginBean implements Serializable {

	private static final long serialVersionUID = 6205682509747036877L;
	private String userName;
	private String passWord;
	private String email;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
