package com.stock.broker.spring.bean_;

import java.io.Serializable;

public class MailBean implements Serializable {

	private static final long serialVersionUID = 7305308170267869050L;
	private String nameCustomer;
	private String mailTo;
	private String subjectMail;
	private String contentMail;
	private String thanks;

	public String getNameCustomer() {
		return nameCustomer;
	}

	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}

	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	public String getSubjectMail() {
		return subjectMail;
	}

	public void setSubjectMail(String subjectMail) {
		this.subjectMail = subjectMail;
	}

	public String getContentMail() {
		return contentMail;
	}

	public void setContentMail(String contentMail) {
		this.contentMail = contentMail;
	}

	public String getThanks() {
		return thanks;
	}

	public void setThanks(String thanks) {
		this.thanks = thanks;
	}

}
