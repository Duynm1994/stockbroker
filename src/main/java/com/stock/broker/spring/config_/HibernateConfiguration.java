package com.stock.broker.spring.config_;

import static com.stock.broker.spring.constant_.CommonConstant.HIBERNATE_DIALECT;
import static com.stock.broker.spring.constant_.CommonConstant.HIBERNATE_FORMAT_SQL;
import static com.stock.broker.spring.constant_.CommonConstant.HIBERNATE_SHOW_SQL;
import static com.stock.broker.spring.constant_.CommonConstant.JDBC_DRIVERCLASSNAME;
import static com.stock.broker.spring.constant_.CommonConstant.JDBC_PAZZWORD;
import static com.stock.broker.spring.constant_.CommonConstant.JDBC_URL;
import static com.stock.broker.spring.constant_.CommonConstant.JDBC_USERNAME;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.stock.broker.spring.config_" })
@PropertySource(value = { "classpath:database-config.properties" })
public class HibernateConfiguration {

	@Autowired
	private Environment environment;

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan("com.stock.broker.spring.entity_");
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty(JDBC_DRIVERCLASSNAME));
		dataSource.setUrl(environment.getRequiredProperty(JDBC_URL));
		dataSource.setUsername(environment.getRequiredProperty(JDBC_USERNAME));
		dataSource.setPassword(environment.getRequiredProperty(JDBC_PAZZWORD));
		return dataSource;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put(HIBERNATE_DIALECT, environment.getRequiredProperty(HIBERNATE_DIALECT));
		properties.put(HIBERNATE_SHOW_SQL, environment.getRequiredProperty(HIBERNATE_SHOW_SQL));
		properties.put(HIBERNATE_FORMAT_SQL, environment.getRequiredProperty(HIBERNATE_FORMAT_SQL));
		properties.put(JDBC_USERNAME, environment.getRequiredProperty(JDBC_USERNAME));
		properties.put(JDBC_PAZZWORD, environment.getRequiredProperty(JDBC_PAZZWORD));
		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}
