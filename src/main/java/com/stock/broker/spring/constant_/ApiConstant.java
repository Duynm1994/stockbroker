package com.stock.broker.spring.constant_;

public class ApiConstant {

	private ApiConstant() {

	}
	
	public static final String TEXT_PLAIN_UTF8 = "text/plain;charset=UTF-8";
	public static final String REDIRECT = "redirect:/";

	// login page
	public static final String URL_REQUEST_MAPPING_LOGIN = "/login";
	public static final String NAME_LOGIN_PAGE = "loginPage";

	// home page
	public static final String URL_REQUEST_MAPPING_HOME_PAGE = "/homePage";
	public static final String NAME_HOME_PAGE = "homePage";

	// reset page
	public static final String NAME_RESET_PAGE = "resetPage";

	// contact page
	public static final String URL_REQUEST_CONTACT_PAGE = "/contactPage";
	public static final String NAME_CONTACT_PAGE = "contactPage";

	// reset PassWord PAGE
	public static final String URL_REQUEST_RESET_PAZZWORD_PAGE = "/resetPassWord";
	public static final String RESET_PAZZWORD_PAGE = "resetPage";
	
	// create new a count page
	public static final String URL_REQUEST_CREATE_NEW_ACCOUNT_PAGE = "/newAccount";
	public static final String NAME_CREATE_NEW_ACCOUNT_PAGE = "createAccountPage";
	
	// get password
	public static final String URL_REQUEST_GET_PAZZWORD = "/getPassWord";
}
