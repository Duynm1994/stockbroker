package com.stock.broker.spring.constant_;

public class CommonConstant {

	private CommonConstant() {

	}

	public static final String URL_TEST = "/test";

	// value int
	public static final int INT_0 = 0;
	public static final int INT_8 = 8;
	public static final int INT_10 = 10;

	// value String
	public static final String UTF_8 = "UTF-8";

	// send mail
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
	public static final String MAIL_USERNAME = "mail.username";
	public static final String MAIL_PAZZWORD = "mail.password";
	public static final String SEND_MAIL_SUCCESS = "Send mail success!";
	public static final String SEND_MAIL_FAILED = "Send mail failed! ";
	public static final String MAIL_DEAR = "mail.dear";
	public static final String MAIL_THANKS = "mail.thanks";
	public static final String MAIL_PAZZWORD_SUBJECT = "mail.password.subject";
	public static final String MAIL_PAZZWORD_RESET = "mail.password.reset";
	public static final String RESET_PAZZWORD_SUCCESS = "reset.password.success";
	public static final String MAIL_INCORRECT_FORMAT = "mail.incorrect.format";
	public static final String ACCOUNT_REGISTR_SUCCESS = "account.registr.success";
	public static final String ACCOUNT_REGISTR_ERROR = "account.registr.error";

	public static final String STRING_EMPTY = "";

	// path
	public static final String PATH_MESSAGE_PROPERTIES = "classpath:messages.properties";
	
	
	// connect database
	public static final String JDBC_DRIVERCLASSNAME = "jdbc.driverClassName";
	public static final String JDBC_URL = "jdbc.url";
	public static final String JDBC_USERNAME = "jdbc.username";
	public static final String JDBC_PAZZWORD = "jdbc.password";
	
	// hibernateProperties
	public static final String HIBERNATE_DIALECT= "hibernate.dialect";
	public static final String HIBERNATE_SHOW_SQL ="hibernate.show_sql";
	public static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
}
