package com.stock.broker.spring.constant_;

public class ErrorConstant {

	private ErrorConstant() {

	}
	
	// error msg
	public static final String ERROR_MSG = "errorMsg";
	
	// error login
	public static final String ERROR_LOG_IN_FAILED = "Log in failed";
	public static final String ERROR_MISSING_USERNAME_OR_PAZZWORD = "error.missing.username.or.password";
	public static final String ERROR_LOGIN_INCORRECT = "error.login.incorrect";
	public static final String ERROR_SYSTEM_LOGIN = "error.system.login";

	// error reset password
	public static final String ERROR_RESET_FAILED = "Reset password failed";
	public static final String ERROR_MISSING_USERNAME_OR_EMAIL = "error.missing.username.or.email";
	public static final String ERROR_NOT_EXIST_USERNAME_OR_MAIL = "error.not.exist.username.or.mail";
	public static final String ERROR_SYSTEM_RESET_PAZZWORD = "error.system.reset.password";

	// error registry account
	public static final String ERROR_NEW_ACOOUNT_SYSTEM = "error.new.acoount.system";
	public static final String ERROR_CREATE_NEW_ACCOUNT_FAILED = "Create new account failed";
	public static final String ERROR_NEW_ACCOUNT_MISSING_FIELD = "error.new.account.missing.field";
	public static final String ERROR_NEW_ACCOUNT_WRONG_EMAIL = "error.new.account.wrong.email";
	public static final String ERROR_NEW_ACCOUNT_WRONG_PAZZWORD = "error.new.account.wrong.password";
	public static final String ERROR_NEW_ACCOUNT_WRONG_PHONE = "error.new.account.wrong.phone";
	public static final String ERROR_NEW_ACCOUNT_WRONG_PAZZWORD_COMFIRM = "error.new.account.wrong.password.comfirm";
	public static final String ERROR_NEW_ACCOUNT_WRONG_USERNAME_EMAIL = "error.new.account.wrong.username.email";
}
