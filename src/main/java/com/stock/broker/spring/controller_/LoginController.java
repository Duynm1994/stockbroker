package com.stock.broker.spring.controller_;

import static com.stock.broker.spring.constant_.ApiConstant.NAME_CREATE_NEW_ACCOUNT_PAGE;
import static com.stock.broker.spring.constant_.ApiConstant.NAME_HOME_PAGE;
import static com.stock.broker.spring.constant_.ApiConstant.NAME_LOGIN_PAGE;
import static com.stock.broker.spring.constant_.ApiConstant.NAME_RESET_PAGE;
import static com.stock.broker.spring.constant_.ApiConstant.REDIRECT;
import static com.stock.broker.spring.constant_.ApiConstant.TEXT_PLAIN_UTF8;
import static com.stock.broker.spring.constant_.CommonConstant.ACCOUNT_REGISTR_ERROR;
import static com.stock.broker.spring.constant_.CommonConstant.ACCOUNT_REGISTR_SUCCESS;
import static com.stock.broker.spring.constant_.CommonConstant.MAIL_INCORRECT_FORMAT;
import static com.stock.broker.spring.constant_.CommonConstant.PATH_MESSAGE_PROPERTIES;
import static com.stock.broker.spring.constant_.CommonConstant.RESET_PAZZWORD_SUCCESS;
import static com.stock.broker.spring.constant_.CommonConstant.STRING_EMPTY;
import static com.stock.broker.spring.constant_.CommonConstant.UTF_8;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_CREATE_NEW_ACCOUNT_FAILED;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_LOGIN_INCORRECT;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_LOG_IN_FAILED;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_MISSING_USERNAME_OR_EMAIL;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_MISSING_USERNAME_OR_PAZZWORD;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_MSG;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACCOUNT_WRONG_USERNAME_EMAIL;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACOOUNT_SYSTEM;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NOT_EXIST_USERNAME_OR_MAIL;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_RESET_FAILED;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_SYSTEM_LOGIN;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_SYSTEM_RESET_PAZZWORD;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ErrorObject;
import com.stock.broker.spring.bean_.ForgotPasswordBean;
import com.stock.broker.spring.bean_.LoginBean;
import com.stock.broker.spring.enum_.QuestionDefault;
import com.stock.broker.spring.service_.LoginService;
import com.stock.broker.spring.util_.ValidateUtil;

@Controller
@PropertySource(value = PATH_MESSAGE_PROPERTIES, encoding = UTF_8)
public class LoginController {

	private static final Logger logger = Logger.getLogger(LoginController.class);

	private static final String QUESTION_LIST = "questionList";

	@Autowired
	private LoginService loginService;

	@Autowired
	Environment env;

	@GetMapping("/")
	public ModelAndView hello(Locale local, ModelAndView mdv) {
		mdv.setViewName("loginPage");
		return mdv;

	}

	@PostMapping(path = "/login", produces = TEXT_PLAIN_UTF8)
	public ModelAndView login(@ModelAttribute("userName") String userName, @ModelAttribute("passWord") String passWord, RedirectAttributes redir) {
		ModelAndView mdv = new ModelAndView();
		boolean flagLogin = false;
		String errorMsg = STRING_EMPTY;
		try {
			if (!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(passWord)) {
				LoginBean loginBean = new LoginBean();
				loginBean.setUserName(userName);
				loginBean.setPassWord(passWord);
				flagLogin = loginService.checkLogin(loginBean);
			} else {
				errorMsg = env.getProperty(ERROR_MISSING_USERNAME_OR_PAZZWORD);
				flagLogin = false;
			}
		} catch (Exception e) {
			errorMsg = env.getProperty(ERROR_SYSTEM_LOGIN);
			logger.debug(ERROR_LOG_IN_FAILED + e.getMessage());
			flagLogin = false;
		}
		if (flagLogin) {
			mdv.setViewName(REDIRECT + NAME_HOME_PAGE);
			redir.addFlashAttribute("userName", userName);
			redir.addFlashAttribute("passWord", passWord);
		} else {
			if (StringUtils.isEmpty(errorMsg)) {
				errorMsg = env.getProperty(ERROR_LOGIN_INCORRECT);
			}
			mdv.setViewName(NAME_LOGIN_PAGE);
			mdv.addObject(ERROR_MSG, errorMsg);
		}
		return mdv;
	}

	@GetMapping(path = "/getPassWord", produces = TEXT_PLAIN_UTF8)
	public String resetPassWord(Model model) {
		model.addAttribute(QUESTION_LIST, QuestionDefault.createQuestionList());
		return "resetPage";
	}

	@GetMapping(path = "/newAccount", produces = TEXT_PLAIN_UTF8)
	public String createNewAccount(Model model) {
		AccountBean acc = new AccountBean();
		model.addAttribute("accountBean", acc);
		model.addAttribute(QUESTION_LIST, QuestionDefault.createQuestionList());
		return NAME_CREATE_NEW_ACCOUNT_PAGE;
	}

	@GetMapping(path = "/login", produces = TEXT_PLAIN_UTF8)
	public ModelAndView loginPage(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redir) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
		String errorMsg = request.getParameter("errorMsg");
		ModelAndView mdv = new ModelAndView(NAME_LOGIN_PAGE);
		if (errorMsg != null) {
			byte[] bytes = errorMsg.getBytes(StandardCharsets.ISO_8859_1);
			errorMsg = new String(bytes, StandardCharsets.UTF_8);
			mdv.addObject(ERROR_MSG, errorMsg);
			mdv.addObject("flagSuccess", true);
		}
		return mdv;
	}

	@PostMapping(path = "/getPassWord")
	public ModelAndView getPassWord(@ModelAttribute("forgotPasswordBean") ForgotPasswordBean forgotPasswordBean, BindingResult result, Model mode, RedirectAttributes redir) {
		ModelAndView mdv = new ModelAndView();
		boolean flagReset = false;
		String errorMsg = STRING_EMPTY;
		String userName = forgotPasswordBean.getUserName();
		String email = forgotPasswordBean.getEmail();
		String contentQue = forgotPasswordBean.getContentQue();
		try {
			if (!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(email) && !StringUtils.isEmpty(contentQue)) {
				if (ValidateUtil.validateEmail(email)) {
					flagReset = loginService.checkResetPassword(forgotPasswordBean);
				} else {
					errorMsg = env.getProperty(MAIL_INCORRECT_FORMAT);
					flagReset = false;
				}
			} else {
				errorMsg = env.getProperty(ERROR_MISSING_USERNAME_OR_EMAIL);
				flagReset = false;
			}
		} catch (Exception e) {
			errorMsg = env.getProperty(ERROR_SYSTEM_RESET_PAZZWORD);
			logger.debug(ERROR_RESET_FAILED + e.getMessage());
			flagReset = false;
		}

		if (flagReset) {
			mdv.setViewName(REDIRECT + "login");
			errorMsg = env.getProperty(RESET_PAZZWORD_SUCCESS);
			mdv.addObject(ERROR_MSG, errorMsg);
		} else {
			if (StringUtils.isEmpty(errorMsg)) {
				errorMsg = env.getProperty(ERROR_NOT_EXIST_USERNAME_OR_MAIL);
			}
			mdv.setViewName(NAME_RESET_PAGE);
			mdv.addObject(ERROR_MSG, errorMsg);
			mdv.addObject(QUESTION_LIST, QuestionDefault.createQuestionList());
		}
		return mdv;
	}

	@PostMapping(path = "/registryNewAccount")
	public ModelAndView newAccount(@ModelAttribute("accountBean") AccountBean accountBean, BindingResult result, Model mode, RedirectAttributes redirl) {
		ModelAndView mdv = new ModelAndView();
		ErrorObject errorObject = new ErrorObject();
		try {
			ValidateUtil.validateLogin(errorObject, accountBean, env);
			if (errorObject.isValidate() && loginService.checkUserNameAndEmailIsUnique(accountBean)) {
				errorObject.setValidate(false);
				errorObject.setMsg(env.getProperty(ERROR_NEW_ACCOUNT_WRONG_USERNAME_EMAIL));
			} else if (errorObject.isValidate()) {
				errorObject.setValidate(loginService.createNewAccount(accountBean));
			}
		} catch (Exception e) {
			errorObject.setValidate(false);
			errorObject.setMsg(env.getProperty(ERROR_NEW_ACOOUNT_SYSTEM));
			logger.debug(ERROR_CREATE_NEW_ACCOUNT_FAILED + e.getMessage());
		}
		if (errorObject.isValidate()) {
			mdv.setViewName(REDIRECT + "login");
			errorObject.setMsg(env.getProperty(ACCOUNT_REGISTR_SUCCESS));
			mdv.addObject(ERROR_MSG, errorObject.getMsg());
		} else {
			if (StringUtils.isEmpty(errorObject.getMsg())) {
				errorObject.setMsg(env.getProperty(ACCOUNT_REGISTR_ERROR));
			}
			mdv.setViewName(NAME_CREATE_NEW_ACCOUNT_PAGE);
			AccountBean acc = new AccountBean();
			mdv.addObject("accountBean", acc);
			mdv.addObject(QUESTION_LIST, QuestionDefault.createQuestionList());
			mdv.addObject(ERROR_MSG, errorObject.getMsg());
		}
		return mdv;
	}

	@ModelAttribute("account")
	public AccountBean getAccountBean() {
		return new AccountBean();
	}

	@ModelAttribute("forgotPasswordBean")
	public ForgotPasswordBean getForgotPasswordBean() {
		return new ForgotPasswordBean();
	}
}
