package com.stock.broker.spring.controller_;

import static com.stock.broker.spring.constant_.CommonConstant.PATH_MESSAGE_PROPERTIES;
import static com.stock.broker.spring.constant_.CommonConstant.UTF_8;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@PropertySource(value = PATH_MESSAGE_PROPERTIES, encoding = UTF_8)
public class MainController {

	@GetMapping(path = "/homePage")
	public String homePage() {
		return "homePage";
	}

	@GetMapping(path = "/contactPage")
	public String contactPage() {
		return "contactPage";
	}
}
