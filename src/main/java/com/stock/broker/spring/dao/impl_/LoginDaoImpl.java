package com.stock.broker.spring.dao.impl_;

import static com.stock.broker.spring.constant_.CommonConstant.INT_0;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ForgotPasswordBean;
import com.stock.broker.spring.bean_.LoginBean;
import com.stock.broker.spring.dao_.LoginDao;
import com.stock.broker.spring.entity_.AccountUser;
import com.stock.broker.spring.entity_.UserInformation;

@Transactional
@Component
@Repository("loginDao")
public class LoginDaoImpl implements LoginDao {

	private static final Logger logger = Logger.getLogger(LoginDaoImpl.class);
	private static final String USER_NAME = "userName";
	private static final String EMAIL = "email";

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public boolean checkLogin(LoginBean loginBean) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			logger.debug("checkLogin failed!");
			return false;
		}
		String hql = "from AccountUser u where u.userName = :userName and u.pazzword = :pazzword";
		Query query = session.createQuery(hql);
		query.setParameter(USER_NAME, loginBean.getUserName());
		query.setParameter("pazzword", loginBean.getPassWord());
		List<AccountUser> list = query.list();
		return !list.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public boolean checkResetPassword(ForgotPasswordBean forgotPasswordBean) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			logger.debug("checkResetPassword failed!");
			return false;
		}
		String hql = "from AccountUser acc where acc.userName = :userName and acc.email = :email and acc.idQue = :idQue and acc.contentQue = :contentQue";
		Query query = session.createQuery(hql);
		query.setParameter(USER_NAME, forgotPasswordBean.getUserName());
		query.setParameter(EMAIL, forgotPasswordBean.getEmail());
		query.setParameter("idQue", forgotPasswordBean.getIdQue());
		query.setParameter("contentQue", forgotPasswordBean.getContentQue());
		List<UserInformation> list = query.list();
		return !list.isEmpty();
	}

	public boolean updateNewPassWordWhenReset(ForgotPasswordBean forgotPasswordBean, String newPassWord) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			logger.debug("updateNewPassWordWhenReset failed!");
			return false;
		}
		String hql = "update AccountUser u set u.pazzword = :pazzword, u.pazzwordConfirm = :pazzwordConfirm "
				+ "where u.userName = :userName and u.email = :email and u.idQue = :idQue and u.contentQue =:contentQue";
		Query query = session.createQuery(hql);
		query.setParameter("pazzword", newPassWord);
		query.setParameter("pazzwordConfirm", newPassWord);
		query.setParameter(USER_NAME, forgotPasswordBean.getUserName());
		query.setParameter(EMAIL, forgotPasswordBean.getEmail());
		query.setParameter("idQue", forgotPasswordBean.getIdQue());
		query.setParameter("contentQue", forgotPasswordBean.getContentQue());
		int updateCount = query.executeUpdate();
		return updateCount > INT_0;
	}

	@SuppressWarnings("unchecked")
	public boolean checkUserNameAndEmailIsUnique(AccountBean accountBean) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			logger.debug("checkUserNameAndEmailIsUnique failed!");
			return false;
		}
		String hql = "from AccountUser acc where acc.userName = :userName and acc.email = :email";
		Query query = session.createQuery(hql);
		query.setParameter(USER_NAME, accountBean.getUserName());
		query.setParameter(EMAIL, accountBean.getEmail());
		List<AccountBean> list = query.list();
		return list.size() > INT_0;
	}

	public boolean createNewAccount(AccountBean accountBean) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
			AccountUser accUser = new AccountUser();
			accUser.setContentQue(accountBean.getContentQue());
			accUser.setEmail(accountBean.getEmail());
			accUser.setIdQue(accountBean.getIdQue());
			accUser.setPazzword(accountBean.getPazzWord());
			accUser.setPazzwordConfirm(accountBean.getPazzWordConfirm());
			accUser.setUserName(accountBean.getUserName());
			session.save(accUser);
		} catch (Exception e) {
			logger.debug("createNewAccount failed!");
			return false;
		}
		return true;
	}

}
