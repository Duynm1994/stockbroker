package com.stock.broker.spring.dao_;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ForgotPasswordBean;
import com.stock.broker.spring.bean_.LoginBean;

public interface LoginDao {

	public boolean checkLogin(LoginBean loginBean);

	public boolean checkResetPassword(ForgotPasswordBean forgotPasswordBean);

	public boolean updateNewPassWordWhenReset(ForgotPasswordBean forgotPasswordBean, String newPassWord);
	
	public boolean checkUserNameAndEmailIsUnique(AccountBean accountBean);
	
	public boolean createNewAccount(AccountBean accountBean);
}
