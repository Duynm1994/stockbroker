package com.stock.broker.spring.entity_;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "ACCOUNT_USER")
@Component
public class AccountUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER")
	private int idUser;
	@Column(name = "USER_NAME")
	private String userName;
	@Column(name = "PASS_WORD")
	private String pazzword;
	@Column(name = "PASS_WORD_CONFIRM")
	private String pazzwordConfirm;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "ID_QUE")
	private int idQue;
	@Column(name = "CONTENT_QUTE")
	private String contentQue;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPazzword() {
		return pazzword;
	}

	public void setPazzword(String pazzword) {
		this.pazzword = pazzword;
	}

	public String getPazzwordConfirm() {
		return pazzwordConfirm;
	}

	public void setPazzwordConfirm(String pazzwordConfirm) {
		this.pazzwordConfirm = pazzwordConfirm;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdQue() {
		return idQue;
	}

	public void setIdQue(int idQue) {
		this.idQue = idQue;
	}

	public String getContentQue() {
		return contentQue;
	}

	public void setContentQue(String contentQue) {
		this.contentQue = contentQue;
	}

}
