package com.stock.broker.spring.entity_;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "USER_INFORMATION")
@Component
public class UserInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER")
	private int idUser;
	@Column(name = "FULL_NAME")
	private String fullName;
	@Column(name = "USER_NAME")
	private String userName;
	@Column(name = "PAZZ_WORD")
	private String pazzWord;
	@Column(name = "PAZZ_WORD_CONFIRM")
	private String pazzWordConfirm;
	@Column(name = "AVATA")
	private String avatar;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PHONE")
	private String phone;
	@Column(name = "ADDRESS")
	private String address;
	@Column(name = "AGE")
	private int age;
	@Column(name = "POSITION_USER")
	private String positionUser;
	@Column(name = "NUMBER_YEAR_EXP")
	private int numberYearExp;
	@Column(name = "DESCRIPTION")
	private String description;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPazzWord() {
		return pazzWord;
	}

	public void setPazzWord(String pazzWord) {
		this.pazzWord = pazzWord;
	}

	public String getPazzWordConfirm() {
		return pazzWordConfirm;
	}

	public void setPazzWordConfirm(String pazzWordConfirm) {
		this.pazzWordConfirm = pazzWordConfirm;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPositionUser() {
		return positionUser;
	}

	public void setPositionUser(String positionUser) {
		this.positionUser = positionUser;
	}

	public int getNumberYearExp() {
		return numberYearExp;
	}

	public void setNumberYearExp(int numberYearExp) {
		this.numberYearExp = numberYearExp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
