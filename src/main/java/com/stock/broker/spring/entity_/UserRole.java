package com.stock.broker.spring.entity_;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "USER_ROLE")
@Component
public class UserRole {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER_ROLE")
	private int idUserRole;

	@Column(name = "ID_USER")
	private int idUser;
	@Column(name = "TYPE_ROLE")
	private String typeRole;
	@Column(name = "FLAG_ROLE")
	private String flagRole;
	@Column(name = "FLAG_ACTIVE")
	private String flagActive;
	@Column(name = "TIME_ACTIVE_FROM")
	private Timestamp timeActiveFrom;
	@Column(name = "TIME_ACTVIE_tO")
	private Timestamp timeActiveTo;

	public int getIdUserRole() {
		return idUserRole;
	}

	public void setIdUserRole(int idUserRole) {
		this.idUserRole = idUserRole;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getTypeRole() {
		return typeRole;
	}

	public void setTypeRole(String typeRole) {
		this.typeRole = typeRole;
	}

	public String getFlagRole() {
		return flagRole;
	}

	public void setFlagRole(String flagRole) {
		this.flagRole = flagRole;
	}

	public String getFlagActive() {
		return flagActive;
	}

	public void setFlagActive(String flagActive) {
		this.flagActive = flagActive;
	}

	public Timestamp getTimeActiveFrom() {
		return timeActiveFrom;
	}

	public void setTimeActiveFrom(Timestamp timeActiveFrom) {
		this.timeActiveFrom = timeActiveFrom;
	}

	public Timestamp getTimeActiveTo() {
		return timeActiveTo;
	}

	public void setTimeActiveTo(Timestamp timeActiveTo) {
		this.timeActiveTo = timeActiveTo;
	}

}
