package com.stock.broker.spring.entity_;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "USER_SKILL")
@Component
public class UserSkill {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_USER_SKILL")
	private int idUserSkill;

	@Column(name = "ID_USER")
	private int idUser;

	@Column(name = "SKILL")
	private String skill;

	public int getIdUserSkill() {
		return idUserSkill;
	}

	public void setIdUserSkill(int idUserSkill) {
		this.idUserSkill = idUserSkill;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

}
