package com.stock.broker.spring.enum_;

import java.util.ArrayList;
import java.util.List;

public class QuestionDefault {

	private int idQue;
	private String contentQue;

	public int getIdQue() {
		return idQue;
	}

	public void setIdQue(int idQue) {
		this.idQue = idQue;
	}

	public String getContentQue() {
		return contentQue;
	}

	public void setContentQue(String contentQue) {
		this.contentQue = contentQue;
	}

	public static List<QuestionDefault> createQuestionList() {
		List<QuestionDefault> questions = new ArrayList<QuestionDefault>();
		QuestionDefault que1 = new QuestionDefault();
		que1.setIdQue(1);
		que1.setContentQue("Mẹ của bạn sinh năm bao nhiêu?");
		questions.add(que1);

		QuestionDefault que2 = new QuestionDefault();
		que2.setIdQue(2);
		que2.setContentQue("Quê nội bạn ở đâu?");
		questions.add(que2);

		QuestionDefault que3 = new QuestionDefault();
		que3.setIdQue(3);
		que3.setContentQue("Ba số cuối số điện thoại của bạn?");
		questions.add(que3);

		QuestionDefault que4 = new QuestionDefault();
		que4.setIdQue(4);
		que4.setContentQue("Quê bạn có đặc sản gì?");
		questions.add(que4);

		QuestionDefault que5 = new QuestionDefault();
		que5.setIdQue(5);
		que5.setContentQue("Bạn kiếm tiền năm bao nhiêu tuổi?");
		questions.add(que5);

		return questions;
	}

}
