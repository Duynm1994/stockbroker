package com.stock.broker.spring.service.impl_;

import static com.stock.broker.spring.constant_.CommonConstant.MAIL_PAZZWORD_RESET;
import static com.stock.broker.spring.constant_.CommonConstant.MAIL_PAZZWORD_SUBJECT;
import static com.stock.broker.spring.constant_.CommonConstant.MAIL_THANKS;
import static com.stock.broker.spring.util_.GenerateSecurePassword.generatePassword;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ForgotPasswordBean;
import com.stock.broker.spring.bean_.LoginBean;
import com.stock.broker.spring.bean_.MailBean;
import com.stock.broker.spring.dao_.LoginDao;
import com.stock.broker.spring.service_.LoginService;
import com.stock.broker.spring.util_.MailUtil;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDao loginDao;

	@Autowired
	private Environment env;

	public boolean checkLogin(LoginBean loginBean) {
		return loginDao.checkLogin(loginBean);
	}

	public boolean checkResetPassword(ForgotPasswordBean forgotPasswordBean) {
		boolean isReset = loginDao.checkResetPassword(forgotPasswordBean);
		boolean isSendMail = false;
		// send mail
		if (isReset) {
			// create new pass word
			String newPassWord = generatePassword();
			// update to db
			boolean isUpdatePassWordReset = loginDao.updateNewPassWordWhenReset(forgotPasswordBean, newPassWord);
			if (isUpdatePassWordReset) {
				// send mail
				MailBean mailBean = new MailBean();
				mailBean.setSubjectMail(env.getProperty(MAIL_PAZZWORD_SUBJECT));
				mailBean.setNameCustomer(forgotPasswordBean.getUserName());
				mailBean.setThanks(env.getProperty(MAIL_THANKS));
				mailBean.setMailTo(forgotPasswordBean.getEmail());
				mailBean.setContentMail(env.getProperty(MAIL_PAZZWORD_RESET) + ": " + newPassWord);
				isSendMail = MailUtil.sendMail(env, mailBean);
			}
		}
		return isReset && isSendMail;
	}

	public boolean checkUserNameAndEmailIsUnique(AccountBean accountBean) {
		return loginDao.checkUserNameAndEmailIsUnique(accountBean);
	}

	public boolean createNewAccount(AccountBean accountBean) {
		return loginDao.createNewAccount(accountBean);
	}

}
