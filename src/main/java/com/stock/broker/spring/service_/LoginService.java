package com.stock.broker.spring.service_;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ForgotPasswordBean;
import com.stock.broker.spring.bean_.LoginBean;

public interface LoginService {

	public boolean checkLogin(LoginBean loginBean);

	public boolean checkResetPassword(ForgotPasswordBean forgotPasswordBean);

	public boolean checkUserNameAndEmailIsUnique(AccountBean accountBean);
	
	public boolean createNewAccount(AccountBean accountBean);

}
