package com.stock.broker.spring.util_;

import java.security.SecureRandom;
import java.util.Random;

public class GenerateSecurePassword {

	private GenerateSecurePassword() {

	}

	private static final Random RANDOM = new SecureRandom();
	private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final int LENGTH_PASSWORD = 8;

	public static String generatePassword() {
		StringBuilder returnValue = new StringBuilder(LENGTH_PASSWORD);
		for (int i = 0; i < LENGTH_PASSWORD; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		return new String(returnValue);
	}

}
