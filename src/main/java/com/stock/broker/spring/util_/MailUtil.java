package com.stock.broker.spring.util_;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.core.env.Environment;

import com.stock.broker.spring.bean_.MailBean;
import com.stock.broker.spring.constant_.CommonConstant;

public class MailUtil {

	private static final Logger logger = Logger.getLogger(MailUtil.class);

	public static void main(String[] args) {
		MailBean mailBean = new MailBean();
		mailBean.setContentMail("Xin chao VN");
		mailBean.setMailTo("nmdt94@gmail.com");
		mailBean.setNameCustomer("CR7");
		mailBean.setSubjectMail("MAIL SUBJECT");
		mailBean.setThanks("Chan thanh cam on");
	}

	public static boolean sendMail(Environment env, MailBean mailBean) {
		final String userName = env.getProperty(CommonConstant.MAIL_USERNAME); // mail
		final String password = env.getProperty(CommonConstant.MAIL_PAZZWORD);
		String mailTo = mailBean.getMailTo();
		// Get the session object
		Properties props = new Properties();
		props.put(CommonConstant.MAIL_SMTP_HOST, env.getProperty(CommonConstant.MAIL_SMTP_HOST));
		props.put(CommonConstant.MAIL_SMTP_PORT, env.getProperty(CommonConstant.MAIL_SMTP_PORT));
		props.put(CommonConstant.MAIL_SMTP_AUTH, env.getProperty(CommonConstant.MAIL_SMTP_AUTH));
		props.put(CommonConstant.MAIL_SMTP_STARTTLS_ENABLE, env.getProperty(CommonConstant.MAIL_SMTP_STARTTLS_ENABLE));

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});
		// Compose the message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setHeader("Content-Type", "text/html; charset=UTF-8");
			message.setFrom(new InternetAddress(userName));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
			message.setSubject(mailBean.getSubjectMail(), "UTF-8");
			message.setText(env.getProperty(CommonConstant.MAIL_DEAR) + "<br />" + mailBean.getNameCustomer() + "<br /> <br />" + mailBean.getContentMail() + "<br /><br />"
					+ mailBean.getThanks(), "utf-8", "html");
			// send the message
			Transport.send(message);
			logger.debug(CommonConstant.SEND_MAIL_SUCCESS);
			return true;
		} catch (MessagingException e) {
			logger.debug(CommonConstant.SEND_MAIL_FAILED + e.getMessage());
			return false;
		}
	}
}
