package com.stock.broker.spring.util_;

import static com.stock.broker.spring.constant_.CommonConstant.INT_8;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACCOUNT_MISSING_FIELD;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACCOUNT_WRONG_EMAIL;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACCOUNT_WRONG_PAZZWORD;
import static com.stock.broker.spring.constant_.ErrorConstant.ERROR_NEW_ACCOUNT_WRONG_PAZZWORD_COMFIRM;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import com.stock.broker.spring.bean_.AccountBean;
import com.stock.broker.spring.bean_.ErrorObject;

public class ValidateUtil {

	private ValidateUtil() {

	}

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public static boolean validateFormRegistry(AccountBean acc) {
		return !StringUtils.isEmpty(acc.getUserName()) && !StringUtils.isEmpty(acc.getPazzWord()) && !StringUtils.isEmpty(acc.getPazzWordConfirm()) && !StringUtils.isEmpty(acc.getEmail())
				&& !StringUtils.isEmpty(acc.getContentQue());
	}

	public static boolean validatePassWordAndPassWordConfirm(AccountBean accountBean) {
		return accountBean.getPazzWord().equals(accountBean.getPazzWordConfirm());
	}

	public static void validateLogin(ErrorObject errorObject, AccountBean accountBean, Environment env) {
		if (!ValidateUtil.validateFormRegistry(accountBean)) {
			errorObject.setValidate(false);
			errorObject.setMsg(env.getProperty(ERROR_NEW_ACCOUNT_MISSING_FIELD));
		} else {
			if (!ValidateUtil.validateEmail(accountBean.getEmail())) {
				errorObject.setValidate(false);
				errorObject.setMsg(env.getProperty(ERROR_NEW_ACCOUNT_WRONG_EMAIL));
			} else {
				if (accountBean.getPazzWord().length() < INT_8) {
					errorObject.setValidate(false);
					errorObject.setMsg(env.getProperty(ERROR_NEW_ACCOUNT_WRONG_PAZZWORD));
				} else {
					if (!ValidateUtil.validatePassWordAndPassWordConfirm(accountBean)) {
						errorObject.setValidate(false);
						errorObject.setMsg(env.getProperty(ERROR_NEW_ACCOUNT_WRONG_PAZZWORD_COMFIRM));
					}
				}
			}
		}
	}

}
