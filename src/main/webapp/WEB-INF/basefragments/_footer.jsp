<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/footer.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid text-center text-md-left" id="footer">
	<!-- <div class="container text-center text-md-left" id="footer"> -->
		<div class="row">
			<div class="col-md-6">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4"><spring:message code="about.us" /></h5>
				<p><spring:message code="about.us1" /></p>
				<p><spring:message code="about.us2" /></p>
				<p><spring:message code="about.us3" /></p>
				<p><spring:message code="about.us4" /></p>
			</div>
			<div class="col-md-3">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4"><spring:message code="footer.link.web" /></h5>
				<ul class="list-unstyled">
					<li><a href="homePage"><spring:message code="title.home" /></a></li>
					<li><a href="#service"><spring:message code="title.service" /></a> </li>
					<li><a href="#news"><spring:message code="title.news" /></a></li>
					<li><a href="contactPage"><spring:message code="title.contact" /></a></li>
					<li><a href="/StockBrokerDEV/"><spring:message code="title.login" /></a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="font-weight-bold text-uppercase mt-3 mb-4"><spring:message code="footer.signup.acc" /></h5>
				<ul class="list-unstyled">
					<li>
						<h5 class="mb-1"><spring:message code="footer.click.signup" /></h5>
					</li>
					<li>
						<a href="newAccount"class="btn btn-danger btn-rounded"><spring:message code="footer.signup" /></a>
					</li>
				</ul>
			</div>
			<div class="col-md-12"><center><spring:message code="footer.copyright" /><a href=""><spring:message code="footer.email" /></a></center></div>
		</div>
	</div>
</body>
</html>
