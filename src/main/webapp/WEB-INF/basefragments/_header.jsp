<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/header.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid" id="header">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<center><img id="logoImg" alt="logo" src="<%=pathWebcontent%>/img/logo.jpg" width="130px" height="130px"></center>
			</div>
			<div class="col-md-8">
				<div class="scrollmenu">
					<a href="homePage"><spring:message code="title.home" /></a> 
					<a href="#servic"><spring:message code="title.service" /></a> 
					<a href="#news"><spring:message code="title.news" /></a> 
					<a href="contactPage"><spring:message code="title.contact" /></a> 
					<a href="/StockBrokerDEV/">
						<img id="logoLogin" alt="logo" src="<%=pathWebcontent%>/img/logo-login.jpg" width="70px" height="70px">
						<spring:message code="title.login" />
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
 
