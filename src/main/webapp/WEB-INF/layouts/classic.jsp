<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<title><tiles:getAsString name="title" /></title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.js" />
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<tiles:insertAttribute name="header" />
			<tiles:insertAttribute name="body" />
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>