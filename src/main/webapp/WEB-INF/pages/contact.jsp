<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/contact.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class ="col-md-12 text-center" id="my-contact">
				<p><spring:message code="contact.description1" /></p>
				<p><spring:message code="contact.description2" />!</p>
				<p><spring:message code="contact.description3" /></p>
				<p><spring:message code="contact.name.admin" /></p>
				<p><spring:message code="contact.phone" /></p>
				<p><spring:message code="contact.email" /></p>
			</div>
		</div>
	</div>
</body>
</html>