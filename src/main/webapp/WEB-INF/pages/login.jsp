<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/login.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="login-form">
		<c:if test="${!errorMsg.equals('')}">
			<c:if test="${flagSuccess}">
				<p style="color: blue;">${errorMsg}<p>
			</c:if>
			<c:if test="${!flagSuccess}">
				<p style="color: red;">${errorMsg}<p>
			</c:if>
		</c:if>
	    <form action="/StockBrokerDEV/login" method="post">
	        <h2 class="text-center"><spring:message code="login.title" /></h2>       
	        <div class="form-group">
	        	<label><spring:message code="login.username" /></label>
	            <input type="text" name ="userName" class="form-control" placeholder="<spring:message code="login.username" />">
	        </div>
	        <div class="form-group">
	        	<label><spring:message code="login.password" /></label>
	            <input type="password" name ="passWord" class="form-control" placeholder="<spring:message code="login.password" />">
	        </div>
	        <div class="form-group">
	            <button type="submit" class="btn btn-primary btn-block"><spring:message code="login.title" /></button>
	        </div>
	        <div class="clearfix">
	            <a href="getPassWord" class="pull-right"><spring:message code="login.forgot.pasword" /></a>
	        </div>        
	    </form>
	    <p class="text-center"><a href="newAccount"><spring:message code="create.new.account" /></a></p>
	</div>
</body>
</html>