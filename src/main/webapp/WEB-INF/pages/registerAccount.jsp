<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns:th="http://www.w3.org/1999/xhtml" xmlns:sf="http://www.w3.org/1999/xhtml" lang="en">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/login.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="login-form">
		<c:if test="${!errorMsg.equals('')}">
			<p style="color: red;">${errorMsg}
			<p>
		</c:if>
		<form:form action="/StockBrokerDEV/registryNewAccount" method="post" modelAttribute="accountBean" >
			<h2 class="text-center">
				<spring:message code="account.new.account" />
			</h2>
			<div class="form-group">
				<label><spring:message code="account.username" /></label><span style="color: red;">*</span>
				<input type="text" name="userName" class="form-control" placeholder="<spring:message code="account.username" />">
			</div>
			<div class="form-group">
				<label><spring:message code="account.password" /></label><span style="color: red;">*</span>
				<input type="password" name="pazzWord" class="form-control" placeholder="<spring:message code="account.password" />">
			</div>
			<div class="form-group">
				<label><spring:message code="account.password.confirm" /></label><span style="color: red;">*</span>
				<input type="password" name="pazzWordConfirm" class="form-control" placeholder="<spring:message code="account.password" />">
			</div>
			<div class="form-group">
				<label><spring:message code="account.email" /></label><span style="color: red;">*</span>
				<input type="text" name="email" class="form-control" placeholder="<spring:message code="account.email" />">
			</div>
			<div class = "form-group">
				<label><spring:message code="account.question" /></label><span style="color: red;">*</span>
				<form:select path="idQue" items="${questionList}" itemValue="idQue" itemLabel="contentQue" />
			</div>
			
			<div class="form-group">
				<label><spring:message code="account.answer" /></label><span style="color: red;">*</span>
				<input type="text" name="contentQue" class="form-control">
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block">
					<spring:message code="account.registry" />
				</button>
			</div>
		</form:form>
	</div>
</body>
</html>