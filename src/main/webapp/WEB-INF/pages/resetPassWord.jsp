<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%
	String pathWebcontent = request.getContextPath();
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/css/login.css" />
<link rel="stylesheet" type="text/css" href="<%=pathWebcontent%>/lib/bootstrap.min.css" />
<script src="<%=pathWebcontent%>/lib/bootstrap.min.js"></script>
</head>
<body>
	<div class="login-form">
		<c:if test="${!errorMsg.equals('')}">
			<p style="color: red;">${errorMsg}
			<p>
		</c:if>
		<form:form action="/StockBrokerDEV/getPassWord" method="post" modelAttribute="forgotPasswordBean">
			<h2 class="text-center">
				<spring:message code="reset.pass.word" />
			</h2>
			<div class="form-group">
				<label><spring:message code="reset.username" /></label>
				<input type="text" name="userName" class="form-control" placeholder="<spring:message code="reset.username" />">
			</div>
			<div class="form-group">
				<label><spring:message code="reset.email" /></label>
				<input type="text" name="email" class="form-control" placeholder="<spring:message code="reset.email" />">
			</div>
			<div class = "form-group">
				<label><spring:message code="account.question" /></label><span style="color: red;">*</span>
				<form:select path="idQue" items="${questionList}" itemValue="idQue" itemLabel="contentQue" />
			</div>
			<div class="form-group">
				<label><spring:message code="account.answer" /></label><span style="color: red;">*</span>
				<input type="text" name="contentQue" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block">
					<spring:message code="reset.get.pass.word" />
				</button>
			</div>
		</form:form>
	</div>
</body>
</html>